module capacitor;

import std.file, std.stdio, std.process;


void exportExecutableFile(string file_in, string file_out)
{
    if (!std.file.exists(file_out)) {
        std.file.write(file_out, file_in);
        if (std.file.exists(file_out)) {
            string cln = "chmod 777 "; //TODO: confirm how Windows behaves
            cln ~= file_out;
            writeln(cln);
            executeShell(cln); //TODO: error handling
        }
    }
}


class Capacitor
{
    import std.string,
    std.stdio,
    std.algorithm, //splitter
    std.file,
    std.path,
    std.exception;
    
    import detached;
    
    string _url, _path, _vcs, _repo;
    
    this(string url, string path) 
    in {
        enforce(path.isDir, "Invalid path: (Capacitor) " ~ path);
    }
    out {
        assert(path.isDir, "Invalid path: (Capacitor) " ~ _path);        
        assert(_vcs.exists, "Invalid VCS file: (Capacitor) " ~ _vcs);
        enforce(_repo.isDir, "Invalid repo dir: (Capacitor) " ~ _repo);
    }
    body  {
        _url = url;
        _path = path;
        std.file.mkdirRecurse(_path);
        _vcs = _path ~ "vcs";  
        static immutable string file_resource = import("git");
        exportExecutableFile(file_resource, _vcs);
        _repo = absolutePath(_path ~ splitter(splitter(_url,'/').back,'.').front);
        writeln("----- Capacitor ----- \n - vcs: ", _vcs, "\n - url: ", _url, "\n - path: ", _path, "\n - repo: ", _repo); 
        if (!exists(_repo)) {
            clone();
        } else {
           // pull();
            }
    }
     
    private void clone()
    in {
        enforce(_path.isDir, "Invalid path: (clone) " ~ _path);        
        enforce(_vcs.exists, "Invalid VCS file: (clone) " ~ _vcs);
    } 
    body {
        std.file.chdir(_path);
        writeln(std.file.getcwd());
        string[] cmdl;
        cmdl ~= "git";
        cmdl ~= "clone";
        cmdl ~= _url;
        spawnProcessDetached(cmdl);
        //TODO: attach stdio to process to capture
    }
    
    void commit(string[] files, string note = "") {
    //add/commit
        std.file.chdir(_path);
    
        string add = _vcs ~ " add";
        foreach (string file; files) {
            add ~= std.string.format(" %s", file);
        
            auto ad = executeShell(add);
            if (ad.status != 0) writeln("Failed to add files");
            else writeln(ad.output);
        }
        string commit = _vcs ~ " commit -m ";
        auto cmt = executeShell(std.string.format("%s\"%s\"",commit,note));
        if (cmt.status != 0) writeln("Failed to commit files");
        else writeln(cmt.output);
    }
    
    void ignore(string[] files) {
    //append files, and commit .gitignore
    
    }
    
    string getFile(string file) {
    
        return "implement me";
    }




}
