//rdmd -main -unittest machine.d

module machine;

import std.variant;

class Machine
{
    private Variant[string] properties;

    this()
    {
        import core.cpuid, std.system, std.socket, std.uuid, std.parallelism;
        properties["run_instance"] = "\""~randomUUID().toString()~"\"";
        properties["host_name"] = Socket.hostName;
        properties["os_type"] = std.system.os;
        int os_cpus = totalCPUs;
        properties["os_cpus"] = os_cpus;
        properties["cpu_cores"] = coresPerCPU();
        properties["cpu_threads"] = threadsPerCPU();
        defaultPoolThreads(os_cpus - (os_cpus / 4)); //make nice
        taskPool();
        properties["threadpool_setting"] = defaultPoolThreads();
    }

    Variant[string] getInstanceProperties()
    {
        return properties;
    }
} 

unittest
{
    import std.stdio, std.conv;
    Machine mymachine =  new machine;
    auto properties = mymachine.getInstanceProperties();
    writefln("instance: \t%s\nhost: \t%s\nos: \t%s\ncpu: \t%s\ncore: \t%s\nthread:\t%s\npool: \t%s",
    properties["run_instance"].to!string, properties["host_name"].to!string,
    properties["os_type"].to!string, properties["os_cpus"].to!string,
    properties["cpu_cores"].to!string, properties["cpu_threads"].to!string,
    properties["threadpool_setting"].to!string);
}
