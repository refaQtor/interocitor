module interocitor;


/++

watchdog loop 
    - pull/reads system rates/active machines
    - captures response time from repo server
    - adjusts heartbeat rate, polling rates, etc base on group state (response time, how many active machines, etc)
    - check remove dead machines
    - update system rates
    - update self state
    - pull/commit/push
    
insert self
    - add all new keys keyed with instance uuid

remove self
    - remove all keys keyed with instance uuid

take job(s)
    - attempt to commit self-id on git-hash for a branch hash
    - if commit clashes \, it means another got it, so try again

do jobs (divvy among cores for single thread processes)
    - checkout branch
    - do calcs
    - commit branch
    
+/

class Interocitor
{
    import core.thread, std.string, std.stdio, std.file, std.variant, std.conv;
    import standardpaths, resusage;
    import capacitor, oscillator, machine;

    
    Machine instance;
    
    
    //     Capacitor master;
    //     Capacitor control;

    Capacitor[string] process;

    Variant[string] _resource_loads;
//          auto _cpuWatcher = new SystemCPUWatcher();
//          auto _memInfo = systemMemInfo();

    double _refresh = 10;

    static private immutable string application_folder = "/interocitor";
    //     static private immutable string master_folder =
    //     "/master/";
    //     static private immutable string control_folder =
    //     "/control/";

    static private immutable string process_folder = "/process/";

    //     string masterDirectory() {
    //         return std.string.format("%s%s%s",writablePath(StandardPath.Config), application_folder,master_folder);
    //     }
    //     
    //     string controlDirectory() {
    //         return std.string.format("%s%s%s",writablePath(StandardPath.Data), application_folder,control_folder);
    //     }

    this(string url = "")
    {
        writeln("Interocitor");
        instance = new Machine;
        writeln(instance.getInstanceProperties());
        
        setResourceStatus(_resource_loads);

        //         master = new Capacitor("https://gitlab.com/interocitor/i-m.git",
        //         masterDirectory());
        //         
        //         control = new Capacitor("https://gitlab.com/interocitor/i-c.git",
        //         controlDirectory());

        process["one"] = new Capacitor("https://gitlab.com/interocitor/i-w.git", processDirectory());

        //start system thread to check for system updates, status sending, 
        //- base polling rate on number of active instances reported in system repo

        //start system thread to check for and pick up jobs according to your threads/cores

        //start system thread to check for and identify and merge results of quick-run_instance

        //start system thread to check for dead machines to move them off the active list

        //

        run();
    }

    void updateRunSettings()
    {
        //get values from master 

        //set values according to master

    }

    void run()
    {

        while (true)
        {
            setResourceStatus(_resource_loads);
            updateRunSettings();
            // auto watchdog_time = to!int(settings["watchdog_time"]);
            Thread.sleep(dur!("seconds")(5)); // watchdog_time ) ); 
        }
    }

    string processDirectory()
    {
        return std.string.format("%s%s%s", writablePath(StandardPath.cache),
            application_folder, process_folder);
    }



    void setResourceStatus(ref Variant[string] loads)
    in
    {
    }
    out
    {
//         assert(loads["cpu_percent"] <= 100);
//         assert(loads["mem_percent"] <= 100);
//         assert(loads["cpu_percent"] >= 0);
//         assert(loads["mem_percent"] >= 0);
//         assert(loads["mem_used"] >= 0);
    }
    body
    {
        try
        {
            auto cpuWatcher = new SystemCPUWatcher();
             loads["cpu_percent"] = cpuWatcher.current();
//             loads["mem_used"] = memInfo.usedRAM;
//             loads["mem_percent"] = memInfo.usedRAMPercent;
        }
        catch (Exception e)
        {
            //consume, don't care here, now
        }
        debug
        {
//             Variant standby = 0.1;
//             writefln("cpu_percent: \t"
//             , loads.get("cpu_percent", standby).peek!(double)
//             ,"\nmem_used: \t"
//             ,loads.get("mem_used", standby).peek!(double)
//             ,"\nmem_percent: \t"
//             ,loads.get("mem_percent", standby).peek!(double));
        }
    }
}
